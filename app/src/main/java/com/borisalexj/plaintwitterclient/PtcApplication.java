package com.borisalexj.plaintwitterclient;

import android.app.Application;
import android.util.Log;

import com.borisalexj.plaintwitterclient.utils.Constants;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import io.fabric.sdk.android.Fabric;

/**
 * Created by user on 5/10/2017.
 */

public class PtcApplication extends Application {
    private String TAG = Constants.TAG + this.getClass().getSimpleName() + " ";

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate: ");
        super.onCreate();

        TwitterAuthConfig authConfig = new TwitterAuthConfig(Constants.TWITTER_KEY, Constants.TWITTER_SECRET);

//        Fabric.with(this, new Twitter(authConfig));
        Fabric.with(this, new Twitter(authConfig), new TweetComposer());

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).build();
        ImageLoader.getInstance().init(config);
    }
}
