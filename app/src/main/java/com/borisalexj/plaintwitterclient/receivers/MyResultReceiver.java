package com.borisalexj.plaintwitterclient.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.borisalexj.plaintwitterclient.utils.Constants;
import com.twitter.sdk.android.tweetcomposer.TweetUploadService;

/**
 * Created by user on 5/9/2017.
 */

public class MyResultReceiver extends BroadcastReceiver {
    private String TAG = Constants.TAG + this.getClass().getSimpleName() + " ";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "onReceive: ");
        if (TweetUploadService.UPLOAD_SUCCESS.equals(intent.getAction())) {
            // success
            final Long tweetId = intent.getExtras().getLong(TweetUploadService.EXTRA_TWEET_ID);
            Log.d("TwitterKit", "onReceive: success");
        } else {
            // failure
            final Intent retryIntent = intent.getExtras().getParcelable(TweetUploadService.EXTRA_RETRY_INTENT);
            Log.d("TwitterKit", "onReceive: failure");
        }
    }
}
