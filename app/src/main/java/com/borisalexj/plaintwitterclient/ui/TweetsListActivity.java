package com.borisalexj.plaintwitterclient.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.borisalexj.plaintwitterclient.R;
import com.borisalexj.plaintwitterclient.adapters.TweetsRecyclerViewAdapter;
import com.borisalexj.plaintwitterclient.utils.Constants;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.core.services.StatusesService;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

/**
 * Created by user on 5/9/2017.
 */
public class TweetsListActivity extends AppCompatActivity {
    private ArrayList<String> mUsersHistorysArray = new ArrayList<>();
    private String TAG = Constants.TAG + this.getClass().getSimpleName() + " ";
    private EditText mUserNameEditText;
    private List<Tweet> mTweetsList = new ArrayList<>();
    private LinearLayoutManager mLinearLayoutManager;
    private RecyclerView mRecyclerView;
    private TweetsRecyclerViewAdapter mAdapter;
    private TwitterApiClient mTwitterApiClient;
    private StatusesService mStatusesService;
    private BroadcastReceiver mBroadcastReceiver;

    private TextView.OnEditorActionListener searchOnEditorDoneActionListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            Log.d(TAG, "onEditorAction: ");
            mUsersHistorysArray.add(String.valueOf(mUserNameEditText.getText()));
            showCurrentUserTimeline();
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            return true;
        }
    };

    private int pastVisiblesItems;
    private int visibleItemCount;
    private int totalItemCount;
    private boolean loading = true;


    private RecyclerView.OnScrollListener mRecyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            if (dy > 0) //check for scroll down
            {
                visibleItemCount = mLinearLayoutManager.getChildCount();
                totalItemCount = mLinearLayoutManager.getItemCount();
                pastVisiblesItems = mLinearLayoutManager.findFirstVisibleItemPosition();
                if (loading) {
                    if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                        loading = false;
                        Log.v(TAG, "Reached last item in list");
                        showTweetsForUser(mUsersHistorysArray.get(mUsersHistorysArray.size() - 1), null, mTweetsList.get(mTweetsList.size() - 1).id);
                        loading = true;
                    }
                }
            }
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: ");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tweets_list);
        mUserNameEditText = (EditText) findViewById(R.id.user_name_editText);

        mUserNameEditText.setOnEditorActionListener(searchOnEditorDoneActionListener);

        mRecyclerView = (RecyclerView) findViewById(R.id.tweets_recyclerView);

        mLinearLayoutManager = new LinearLayoutManager(this);
        mAdapter = new TweetsRecyclerViewAdapter(this, mTweetsList);

        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

        mRecyclerView.addOnScrollListener(mRecyclerViewOnScrollListener);

        mTwitterApiClient = TwitterCore.getInstance().getApiClient();
        mStatusesService = mTwitterApiClient.getStatusesService();
        if (savedInstanceState != null) {
            Log.d(TAG, "onCreate: savedInstanceState != null");
            mUsersHistorysArray = (ArrayList<String>) savedInstanceState.getSerializable(Constants.USER_HISTORY_ARRAY_LIST);
        }

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        View viewForFocus = TweetsListActivity.this.getCurrentFocus();
        if (viewForFocus != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(viewForFocus.getWindowToken(), 0);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        Log.d(TAG, "onSaveInstanceState: ");
        super.onSaveInstanceState(outState);
        outState.putSerializable(Constants.USER_HISTORY_ARRAY_LIST, mUsersHistorysArray);
    }

    @Override
    protected void onResume() {
        Log.d(TAG, "onResume: ");
        super.onResume();
        if (mUsersHistorysArray.size() > 0) {
            showCurrentUserTimeline();
        } else {
            restoreSearch();
        }
//        showTweetsForUser(mCurrentUser, null, null);
        initializeBroadcastReceiver();
    }

    @Override
    protected void onPause() {
        Log.d(TAG, "onPause: ");
        super.onPause();
        try {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mBroadcastReceiver);
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
    }

    private void initializeBroadcastReceiver() {
        Log.d(TAG, "initializeBroadcastReceiver: ");
        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d(TAG, "onReceive: inside receiver");
                if (intent.getAction().equals(Constants.INTENT_SHOW_USER_TIMELINE)) {
                    String newUser = intent.getStringExtra(Constants.INTENT_USERNAME_FIELD);
                    Log.d(TAG, "onReceive: new Username - " + newUser);
                    mUserNameEditText.setText("");
                    mUserNameEditText.append(newUser);
                    mUsersHistorysArray.add(newUser);
                    showCurrentUserTimeline();
                }
            }
        };
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.INTENT_SHOW_USER_TIMELINE);
        LocalBroadcastManager.getInstance(this).registerReceiver(mBroadcastReceiver, intentFilter);
    }

    public void restoreSearchClick(View view) {
        Log.d(TAG, "restoreSearchClick: ");
        restoreSearch();
    }

    private void restoreSearch() {
        Log.d(TAG, "restoreSearch: ");
        mTweetsList = new ArrayList<>();
        if (Twitter.getSessionManager().getActiveSession() != null) {
            String newUser = Twitter.getSessionManager().getActiveSession().getUserName();
            mUserNameEditText.setText("");
            mUserNameEditText.append(newUser);
            mUsersHistorysArray = new ArrayList<>();
            mUsersHistorysArray.add(newUser);
            showTweetsForUser(newUser, null, null);
        } else {
            mUsersHistorysArray = new ArrayList<>();
            mUserNameEditText.setText("");
            mAdapter.setTweetsList(mTweetsList);
        }
    }

    private void showTweetsForUser(String username, Integer count, final Long maxId) {
        Log.d(TAG, "showTweetsForUser: " + username + " " + (maxId != null ? maxId : ""));
        if (count == null) {
            count = 20;
        }
        Call<List<Tweet>> call = mStatusesService.userTimeline(
                null,
                username,
                count,
                null,
                maxId,
                false, false, true, true);
        Log.i("TAG", "showTweetsForUser: " + call.request().url());
        call.enqueue(new Callback<List<Tweet>>() {
            @Override
            public void success(Result<List<Tweet>> result) {
                Log.d("TwitterKit", "success: 1" + result.toString());
                mTweetsList.addAll(result.data);
                if (maxId != null && mTweetsList.size() > 0) {
                    mTweetsList.remove(0); // just because continuation includes record with maxId, that is existed in list already
                }
                mAdapter.setTweetsList(mTweetsList);
            }

            public void failure(TwitterException exception) {
                Log.e("TwitterKit", "failure: 1" + exception.getMessage());
                Toast.makeText(TweetsListActivity.this, "Error getting tweets...", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void addTweetClick(View view) {
        Log.d(TAG, "addTweetClick: ");
        if (Twitter.getSessionManager().getActiveSession() == null) {
            Toast.makeText(this, "You aren't logged in.\nPlease, login on previous screen", Toast.LENGTH_SHORT).show();
            return;
        }

        TweetComposer.Builder builder = new TweetComposer.Builder(this)
                .text("just setting up my Fabric.");
        Toast.makeText(this, "Just a little reminder:\nTweets do not appear immediately after adding.\nPress a \"Refresh\" button in a few seconds", Toast.LENGTH_SHORT).show();
        builder.show();
    }

    public void searchUserTimelineClick(View view) {
        Log.d(TAG, "searchUserTimelineClick: ");
        mUsersHistorysArray.add(String.valueOf(mUserNameEditText.getText()));
        showCurrentUserTimeline();
    }

    private void showCurrentUserTimeline() {
        Log.d(TAG, "showCurrentUserTimeline: ");
        mTweetsList = new ArrayList<>();
        mAdapter.setTweetsList(mTweetsList);
        showTweetsForUser(mUsersHistorysArray.get(mUsersHistorysArray.size() - 1), null, null);
    }

    @Override
    public void onBackPressed() {
        Log.d(TAG, "onBackPressed: ");
        if (mUsersHistorysArray.size() <= 1) {
            finish();
        } else {
            removeLastUserFromHistoryAndSearch();
        }
    }

    private void removeLastUserFromHistoryAndSearch() {
        Log.d(TAG, "removeLastUserFromHistoryAndSearch: ");
        mUsersHistorysArray.remove(mUsersHistorysArray.size() - 1);
        mUserNameEditText.setText(mUsersHistorysArray.get(mUsersHistorysArray.size() - 1));
        showCurrentUserTimeline();
    }

    public void refreshClick(View view) {
        showCurrentUserTimeline();
    }
}
