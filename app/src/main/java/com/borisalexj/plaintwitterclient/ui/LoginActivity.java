package com.borisalexj.plaintwitterclient.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.borisalexj.plaintwitterclient.R;
import com.borisalexj.plaintwitterclient.utils.Constants;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

public class LoginActivity extends AppCompatActivity {
    private String TAG = Constants.TAG + this.getClass().getSimpleName() + " ";

    private TwitterLoginButton mLoginButton;
    private TextView mLoggedAsTextView;
    private Callback<TwitterSession> mLoginCallback = new Callback<TwitterSession>() {
        @Override
        public void success(Result<TwitterSession> result) {
            TwitterSession session = result.data;
            TwitterAuthToken authToken = session.getAuthToken();
            String token = authToken.token;
            String secret = authToken.secret;

            String msg = "@" + session.getUserName() + " logged in!";
            Log.d("TwitterKit", "Login success: " + msg);
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
            startActivity(new Intent(LoginActivity.this, TweetsListActivity.class));
        }

        @Override
        public void failure(TwitterException exception) {
            String msg = "Login with Twitter failure";
            Log.d("TwitterKit", msg, exception);
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if (Twitter.getSessionManager().getActiveSession() != null) {
            startActivity(new Intent(LoginActivity.this, TweetsListActivity.class));
        }
        mLoginButton = (TwitterLoginButton) findViewById(R.id.twitter_login_button);
        mLoggedAsTextView = (TextView) findViewById(R.id.logged_as_textView);
        mLoginButton.setCallback(mLoginCallback);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Pass the activity result to the login button.
        mLoginButton.onActivityResult(requestCode, resultCode, data);
    }

    public void skipLoginClick(View view) {
        startActivity(new Intent(LoginActivity.this, TweetsListActivity.class));
    }

    public void logoutClick(View view) {
        Twitter.logOut();
        showLoggedUserName();
    }

    @Override
    protected void onResume() {
        super.onResume();
        showLoggedUserName();
    }

    private void showLoggedUserName() {
        if (Twitter.getSessionManager().getActiveSession() != null) {
            String userName = Twitter.getSessionManager().getActiveSession().getUserName();
            mLoggedAsTextView.setText(getString(R.string.you_logged_as, userName));
        } else {
            mLoggedAsTextView.setText("");
        }

    }
}
