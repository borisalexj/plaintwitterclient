package com.borisalexj.plaintwitterclient.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.SpannedString;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.borisalexj.plaintwitterclient.R;
import com.borisalexj.plaintwitterclient.utils.Constants;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.twitter.sdk.android.core.models.MentionEntity;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.core.models.UrlEntity;
import com.twitter.sdk.android.tweetui.internal.ClickableLinkSpan;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 5/10/2017.
 */

public class TweetsRecyclerViewAdapter extends RecyclerView.Adapter<TweetsRecyclerViewAdapter.TweetViewHolder> {
    private static final String TWEETS_ARRAY_LIST = "TWEETS_ARRAY_LIST";
    private String TAG = Constants.TAG + this.getClass().getSimpleName() + " ";

    private Context mContext;
    private List<Tweet> mAdapterTweetsList = new ArrayList<>();
    private ImageLoader mImageLoader;

    public TweetsRecyclerViewAdapter(Context context, List<Tweet> tweetsList) {
        Log.d(TAG, "TweetsRecyclerViewAdapter: ");
        mContext = context;
        mAdapterTweetsList = tweetsList;
        mImageLoader = ImageLoader.getInstance(); // Get singleton instance
    }

    public void setTweetsList(List<Tweet> tweetsList) {
        Log.d(TAG, "setTweetsList: ");
        this.mAdapterTweetsList = tweetsList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mAdapterTweetsList == null ? 0 : mAdapterTweetsList.size();
    }

    public void addTweets(List<Tweet> tweetsList) {
        mAdapterTweetsList.addAll(tweetsList);
        notifyDataSetChanged();
    }

    @Override
    public TweetViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        Log.d(TAG, "onCreateViewHolder: ");
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.tweet_item, parent, false);
        TweetViewHolder viewHolder = new TweetViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(TweetViewHolder tvh, int i) {
//        Log.d(TAG, "onBindViewHolder: ");

        Tweet tweet = mAdapterTweetsList.get(i);
        mImageLoader.displayImage(tweet.user.profileImageUrl, tvh.tweetAvatarImageView);

        if (tweet.retweetedStatus != null) {
            tvh.tweetAuthor.setText(mContext.getString(R.string.retweet_author_template, tweet.retweetedStatus.user.screenName, tweet.user.screenName));
        } else {
            tvh.tweetAuthor.setText(mContext.getString(R.string.tweet_author_template, tweet.user.screenName));
        }
        tvh.tweetDateTime.setText(tweet.createdAt);

        SpannedString text = SpannedString.valueOf(tweet.text);
        SpannableStringBuilder builder = new SpannableStringBuilder(text);

        if (tweet.entities.userMentions != null) {
            for (MentionEntity mention : tweet.entities.userMentions) {
                final String screenName = mention.screenName;
                int fromIndex = 0;
                while (tweet.text.indexOf(screenName, fromIndex) != -1) {
                    builder.setSpan(new ClickableSpan() {
                                        @Override
                                        public void onClick(View widget) {
                                            Log.d("TAG", "onClick: ");
                                            Intent intent = new Intent(Constants.INTENT_SHOW_USER_TIMELINE);
                                            intent.putExtra(Constants.INTENT_USERNAME_FIELD, screenName);
                                            LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
                                        }
                                    }, tweet.text.indexOf(screenName, fromIndex),
                            tweet.text.indexOf(screenName, fromIndex) + screenName.length(),
                            Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
                    fromIndex = tweet.text.indexOf(screenName, fromIndex) + screenName.length();
                }
            }
        }

        if (tweet.entities.urls != null) {
            for (UrlEntity url : tweet.entities.urls) {
                final String displayUrl = url.url;
                final String expandedUrl = url.expandedUrl;
                final String _url = url.url;
                int fromIndex = 0;
                while (tweet.text.indexOf(displayUrl, fromIndex) != -1) {
                    builder.setSpan(new ClickableLinkSpan(R.color.colorAccent, R.color.colorPrimary, true) {
                                        @Override
                                        public void onClick(View widget) {
                                            Log.d("TAG", "onClick: " + expandedUrl);
                                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(expandedUrl));
                                            browserIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            mContext.startActivity(browserIntent);
                                        }
                                    }, tweet.text.indexOf(displayUrl, fromIndex),
                            tweet.text.indexOf(displayUrl, fromIndex) + displayUrl.length(),
                            Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                    fromIndex = tweet.text.indexOf(displayUrl, fromIndex) + displayUrl.length();
                }
            }
        }

        tvh.tweetText.setText(builder);
        tvh.tweetText.setMovementMethod(LinkMovementMethod.getInstance());
        tvh.tweetText.setHighlightColor(Color.TRANSPARENT);
        tvh.tweetText.requestLayout();
        tvh.tweetText.invalidate();
    }

    public class TweetViewHolder extends RecyclerView.ViewHolder {
        ImageView tweetAvatarImageView;
        TextView tweetAuthor;
        TextView tweetDateTime;
        TextView tweetText;
        private String TAG = Constants.TAG + this.getClass().getSimpleName() + " ";

        public TweetViewHolder(View itemView) {
            super(itemView);
//            Log.d(TAG, "TweetViewHolder: ");
            tweetAvatarImageView = (ImageView) itemView.findViewById(R.id.tweet_avatar_imageView);
            tweetAuthor = (TextView) itemView.findViewById(R.id.tweet_author);
            tweetDateTime = (TextView) itemView.findViewById(R.id.tweet_date_time);
            tweetText = (TextView) itemView.findViewById(R.id.tweet_text);
        }
    }
}
